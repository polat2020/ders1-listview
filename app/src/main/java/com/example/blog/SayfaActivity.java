package com.example.blog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.blog.databinding.ActivitySayfaBinding;

public class SayfaActivity extends AppCompatActivity {

    ActivitySayfaBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_sayfa);

        binding = ActivitySayfaBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.buttonCustomAdapterList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        CustomAdapterListViewActivity.class);
                startActivity(i);
            }
        });


        binding.buttonListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        ListViewDetailActivity.class);
                startActivity(i);
            }
        });


        binding.buttonScroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        ScrollViewActivity.class);
                startActivity(i);
            }
        });


        binding.buttonHesap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        HesapMakinesiActivity.class);
                startActivity(i);
            }
        });

        binding.buttonRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        RelativeLayoutMainActivity.class);
                startActivity(i);
            }
        });
        binding.buttonTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        TableLayoutMainActivity.class);
                startActivity(i);
            }
        });


        binding.buttonLineer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        LineerMainActivity.class);
                startActivity(i);
            }
        });



        binding.buttonFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        FrameLayoutTestActivity.class);
                startActivity(i);
            }
        });

        binding.buttonListViewNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        ListViewActivity.class);
                startActivity(i);
            }
        });
        binding.buttonGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SayfaActivity.this,
                        GridViewActivity.class);
                startActivity(i);
            }
        });
    }
}