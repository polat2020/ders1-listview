package com.example.blog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.blog.databinding.ActivityDetailBinding;
import com.example.blog.dto.BlogItemDto;

public class DetailActivity extends AppCompatActivity {

    ActivityDetailBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        View view= binding.getRoot();
        setContentView(view);

        Intent intent = getIntent();

        BlogItemDto selectedItem = (BlogItemDto) intent.getSerializableExtra("item");

        binding.txtName.setText(selectedItem.getName());
        binding.txtDetail.setText(selectedItem.getDetail());
        binding.img1.setImageResource(selectedItem.getImage());

    }
}