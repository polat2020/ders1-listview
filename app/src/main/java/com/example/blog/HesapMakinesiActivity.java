package com.example.blog;

import static com.example.blog.enums.IslemTuru.*;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blog.enums.IslemTuru;

public class HesapMakinesiActivity extends AppCompatActivity {

    private EditText edittext1, edittext2;
    private TextView txtResult;
    private Button button1,button2,button3,button4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hesap_makinesi);

        edittext1 = (EditText) findViewById(R.id.editText1);
        edittext2 = (EditText) findViewById(R.id.editText2);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        txtResult = (TextView) findViewById(R.id.txtResult);
        HesapMakinesiActivity that = this;
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                that.Hesapla(Topla);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                that.Hesapla(Cikar);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                that.Hesapla(Carp);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                that.Hesapla(Bol);
            }
        });
    }






    public void Hesapla(IslemTuru islemTuru ) {
        String value1 = edittext1.getText().toString();
        String value2 = edittext2.getText().toString();
        int a = Integer.parseInt(value1);
        int b = Integer.parseInt(value2);
        String result = "" ;
        switch (islemTuru) {
            case Topla:

                result = String.valueOf( a + b);
                break;

            case Cikar:
                result = String.valueOf( a - b);


                break;
            case Carp:
                result = String.valueOf( a*  b);
                break;
            case Bol:
                result = String.valueOf( a / b);
                break;
            default: break;
        }


        txtResult.setText(result);
        Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();

    }
}