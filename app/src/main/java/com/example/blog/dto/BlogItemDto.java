package com.example.blog.dto;

import java.io.Serializable;

public class BlogItemDto implements  Serializable  {
    String name; // isim bilgisi
    String detail; // açıklama
    int image; // resim bilgisi

    public BlogItemDto(String name, String detail, int image) {
        this.name = name;
        this.detail = detail;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
