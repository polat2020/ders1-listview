package com.example.blog;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class AutoCompleteTextViewExamActivity extends AppCompatActivity {

    String[] language ={"C","C++","Java",".NET","iPhone","Android","ASP.NET","PHP"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.select_dialog_item, language);
        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.cmbYazilimTurleri);
        textView.setAdapter(adapter);
        textView.setTextColor(Color.RED);

    }
}