package com.example.blog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.example.blog.databinding.ActivityMainBinding;
import com.example.blog.dto.BlogItemDto;

import java.util.ArrayList;

public class ListViewDetailActivity extends AppCompatActivity {
    ArrayList<BlogItemDto> list;
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        this.Getir();
    }
    public void Getir() {
        list = new ArrayList<>();
        list.add(new BlogItemDto("Item 1", "Item Detay 1", R.drawable.item1));
        list.add(new BlogItemDto("Item 2", "Item Detay 2", R.drawable.item2));
        list.add(new BlogItemDto("Item 3", "Item Detay 3", R.drawable.item3));
        list.add(new BlogItemDto("Item 4", "Item Detay 4", R.drawable.item4));

        ArrayList<String> nameList = new ArrayList<>();
        for (BlogItemDto item : list)
            nameList.add(item.getName());

        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,nameList);
        binding.listView1.setAdapter(arrayAdapter);


        binding.listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ListViewDetailActivity.this,
                        DetailActivity.class);
                intent.putExtra("item", list.get(i));
                startActivity(intent);
            }
        });
    }

/*
    public void Getir() {
        list = new ArrayList<>();
        list.add(new BlogItemDto("Item 1", "Item Detay 1", R.drawable.item1));
        list.add(new BlogItemDto("Item 2", "Item Detay 2", R.drawable.item2));
        list.add(new BlogItemDto("Item 3", "Item Detay 3", R.drawable.item3));


        BlogAdapter blogAdapter = new BlogAdapter(list);
        binding.recyclerView.setAdapter(blogAdapter);
    }*/

}