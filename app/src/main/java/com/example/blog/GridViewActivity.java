package com.example.blog;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class GridViewActivity extends AppCompatActivity {

    ArrayList<String> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        for (int i = 0; i < 100; i++) {
            list.add("Test" + i);
        }

        GridView listGridView = (GridView)findViewById(R.id.simpleGridView);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview_item,
                R.id.textView, list);
        listGridView.setAdapter(arrayAdapter);


    }
}